package at.aau.mmis;

import at.aau.mmis.beans.ExtensiveSearchResult;
import at.aau.mmis.beans.FinalWinner;
import at.aau.mmis.beans.SearchResult;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Optional;

/**
 * Created by phmoll on 15.06.15.
 */
public class RecommenderController {

    private Main main;

    private FinalWinner bestResult;

    private Stage resultsStage;

    private Scene resultsScene;

    private int indexSize;

    @FXML
    private ImageView recommenderImageView;

    @FXML
    private StackPane imageStackPane;

    @FXML
    private Text recommendedText;

    @FXML
    private Button yesBtn;

    @FXML
    private Button noBtn;

    @FXML
    private void loadImage() {
        yesBtn.setDisable(false);
        noBtn.setText("No");

        recommendedText.setText("");
        File file = new File(Constants.IMAGE_PATH_NAME);
        if (file.exists()) {
            file.delete();
        }
        recommenderImageView.setImage(WikipediaService.downloadRandomImageAndGetImageView().getImage());
        recommenderImageView.setPreserveRatio(true);
        recommenderImageView.setSmooth(true);
        recommenderImageView.setCache(true);

        imageStackPane.setAlignment(recommenderImageView, Pos.CENTER);

        ExtensiveSearchResult result = null;

        try {
            result = LireService.findImage(Constants.IMAGE_PATH_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(result != null)
            result.addWinner(result.getWinner().getCategoryEnum().category, result.getWinner().type);

        String results = result == null ? "\nAutoColorCorrelogram doesn't like small images?" : result.toString();
        Text text = new Text(results + "\n\nIndexSize:" + indexSize);
        Scene scene = new Scene(new Group(text), 480, 850);

        text.wrappingWidthProperty().bind(scene.widthProperty().subtract(15));


        resultsStage.setScene(scene);

        if(!resultsStage.isShowing()) {
            resultsStage.show();
        }





        if(result != null) {
            FinalWinner finalWinner = result.determineFinalWinner();

            if(!finalWinner.isClearWinnerDetermined()) {
                yesBtn.setDisable(true);
                noBtn.setText("Specify");
            }
            recommendedText.setText(finalWinner.getText());
            bestResult = finalWinner;
        } else {
            recommendedText.setText("AutoColorCorrelogram is not playing nicely!");
        }

    }

    @FXML
    private void saveImageWithCurrentParams() {

        if (bestResult != null && bestResult.getType() != null && bestResult.getCategory() != null) {
            indexSize = LireService.indexImage(Constants.IMAGE_PATH_NAME,
                    bestResult.getCategory(),
                    bestResult.getType());
            loadImage();
        } else {
            JOptionPane.showMessageDialog(null, "No image selected", "Warning", JOptionPane.ERROR_MESSAGE);
        }

    }

    @FXML
    private void showDialog() {
        if (bestResult == null) {
            JOptionPane.showMessageDialog(null, "No image selected", "Warning", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Classify Image");
        dialog.setHeaderText("What do you see on the image?");

// Set the button types.
        ButtonType loginButtonType = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

// Create the category and type labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        ComboBox<String> category = new ComboBox<>();
        category.getItems().addAll(CategoryEnum.getCategories());



        ComboBox<String> type = new ComboBox<>();
        type.getItems().addAll(CategoryEnum.getInstance(category.getItems().get(0)).types);

        category.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                type.getItems().clear();
                type.getItems().addAll(CategoryEnum.getInstance(newValue).types);
            }
        });


        grid.add(new Label("Category:"), 0, 0);
        grid.add(category, 1, 0);
        grid.add(new Label("Type:"), 0, 1);
        grid.add(type, 1, 1);

// Enable/Disable login button depending on whether a category was entered.
        Node saveBtn = dialog.getDialogPane().lookupButton(loginButtonType);

        dialog.getDialogPane().setContent(grid);

// Request focus on the category field by default.
        Platform.runLater(() -> category.requestFocus());

// Convert the result to a category-type-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(category.getValue(), type.getValue());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        result.ifPresent(dialogResult -> {
//            System.out.println("Username=" + dialogResult.getKey() + ", Password=" + dialogResult.getValue());
            String categoryValue = dialogResult.getKey();
            String typeValue = dialogResult.getValue();
            if (categoryValue != null && !categoryValue.isEmpty()
                    && typeValue != null && !typeValue.isEmpty()) {
                indexSize = LireService.indexImage(Constants.IMAGE_PATH_NAME, categoryValue, typeValue);
                loadImage();
            }else {
                JOptionPane.showMessageDialog(null, "Category and Type should not be empty", "Warning", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    @FXML
    private void initialize() {
        resultsStage = new Stage();
        resultsStage.setTitle("Results");


    }

    public void setMain(Main main) {
        this.main = main;
    }
}
