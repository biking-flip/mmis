package at.aau.mmis;

import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by phmoll on 15.06.15.
 */
public class WikipediaService {

    public static void downloadRandomImage() {
        try {
            // Open random article
            Document doc = Jsoup.connect("https://en.wikipedia.org/wiki/Special:Random").get();

            // Find thumbimage
            Elements elements = doc.select("img.thumbimage");
            while (elements.isEmpty()) {
                doc = Jsoup.connect("https://en.wikipedia.org/wiki/Special:Random").get();
                elements = doc.select("img.thumbimage");
            }
            // Copy image url
            String imageLocation = elements.get(0).attr("src");
            Connection.Response resultImageResponse = Jsoup.connect("http:" + imageLocation)
                    .ignoreContentType(true).execute();
            // Download-image to images/buffer.jpg
            File f = new File("images"+ File.separator +"buffer.jpg");
            FileOutputStream out = (new FileOutputStream(f));
            out.write(resultImageResponse.bodyAsBytes());  // resultImageResponse.body() is where the image's contents are.
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ImageView downloadRandomImageAndGetImageView() {
        try {
            downloadRandomImage();
            BufferedImage bf = ImageIO.read(new File(Constants.IMAGE_PATH_NAME));
            WritableImage wr = null;
            if (bf != null) {
                wr = new WritableImage(bf.getWidth(), bf.getHeight());
                PixelWriter pw = wr.getPixelWriter();
                for (int x = 0; x < bf.getWidth(); x++) {
                    for (int y = 0; y < bf.getHeight(); y++) {
                        pw.setArgb(x, y, bf.getRGB(x, y));
                    }
                }
            }
            return new ImageView(wr);
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
