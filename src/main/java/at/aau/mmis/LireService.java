package at.aau.mmis;

import at.aau.mmis.beans.Counter;
import at.aau.mmis.beans.ExtensiveSearchResult;
import at.aau.mmis.beans.SearchResult;
import com.google.common.util.concurrent.AtomicLongMap;
import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.filter.RerankFilter;
import net.semanticmetadata.lire.imageanalysis.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import net.semanticmetadata.lire.impl.*;
import net.semanticmetadata.lire.utils.LuceneUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;

import javax.imageio.ImageIO;

/**
 * Created by phmoll on 15.06.15.
 */
public class LireService {


    private static Log log = LogFactory.getLog(LireService.class);

    private static Map<String, Class> histogramMap;

    static {
        histogramMap = new HashMap<>();
//        histogramMap.put(DocumentBuilder.FIELD_NAME_CEDD, CEDD.class);
        histogramMap.put(DocumentBuilder.FIELD_NAME_COLORLAYOUT, ColorLayout.class);
        histogramMap.put(DocumentBuilder.FIELD_NAME_AUTOCOLORCORRELOGRAM, AutoColorCorrelogram.class);
        histogramMap.put(DocumentBuilder.FIELD_NAME_PHOG, PHOG.class);
        histogramMap.put(DocumentBuilder.FIELD_NAME_EDGEHISTOGRAM, EdgeHistogram.class);
        histogramMap.put(DocumentBuilder.FIELD_NAME_LOCAL_BINARY_PATTERNS, LocalBinaryPatterns.class);

    }

    public static int indexImage(String path, String category, String type) {

        try {
            // Before indexing, mv the image to another location
            String uuid = UUID.randomUUID().toString();
            String target = Constants.IMAGE_DIRECTORY + uuid + ".jpg";
            new File(path).renameTo(new File(target));

            BufferedImage image = ImageIO.read(new FileInputStream(target));
            ChainedDocumentBuilder chainedDocumentBuilder = new ChainedDocumentBuilder();

            for (Map.Entry<String, Class> entry : histogramMap.entrySet())
            {
                chainedDocumentBuilder.addBuilder(new GenericDocumentBuilder(entry.getValue()));
            }

            IndexWriterConfig conf = new IndexWriterConfig(LuceneUtils.LUCENE_VERSION,
                    new WhitespaceAnalyzer(LuceneUtils.LUCENE_VERSION));
            IndexWriter iw = new IndexWriter(FSDirectory.open(new File(Constants.INDEX_LOCATION)), conf);
            Document document = chainedDocumentBuilder.createDocument(image, target);
            document.add(new TextField("category", category, Field.Store.YES));
            document.add(new StringField("type", type, Field.Store.YES));
            iw.addDocument(document);
            int numberOfDocs = iw.numDocs();
            iw.close();

            log.info("Added image to index (" + category + " - " + type + ")");
            return numberOfDocs;


        }catch (IOException ex) {
            ex.printStackTrace();

        }
        return 0;
    }

    public static ExtensiveSearchResult findImage(String path) {
        try {
            log.info("Searching for image " + path);

            BufferedImage img = ImageIO.read(new FileInputStream(path));
            IndexReader ir = DirectoryReader.open(FSDirectory.open(new File(Constants.INDEX_LOCATION)));

            ImageSearcher searcher = new GenericFastImageSearcher(10, AutoColorCorrelogram.class);

            Map<String, List<SearchResult>> resultsMap = getAllFeaturesResultMap(searcher, ir, img);
//
//  List<SearchResult> results = getResultList(searcher, ir, img);



//
            List<SearchResult> finalList = new ArrayList<>();
            for (Map.Entry<String, List<SearchResult>> entry : resultsMap.entrySet())
            {
                log.info(entry.getKey() + " :" + entry.getValue().get(0));
                finalList.add(entry.getValue().get(0));
            }

            Collections.sort(finalList);

            ExtensiveSearchResult exResult = generateExtensiveSearchResult(resultsMap);
            exResult.setWinner(finalList.get(0));

            return exResult;
//            if (!finalList.isEmpty()) {
//                return finalList.get(0);
//            }


        }catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static List<SearchResult> getResultList(ImageSearcher searcher, IndexReader ir, BufferedImage img) throws IOException {
        // searching with a image file ...
        ImageSearchHits hits = searcher.search(img, ir);
        // searching with a Lucene document instance ...
//        ImageSearchHits hits = searcher.search(ir.document(0), ir);

        log.info(hits.length() + " images found");

        List<SearchResult> results = makeList(hits, ir);

        Collections.sort(results);

        return results;
    }

    private static void logResults(ImageSearchHits hits, IndexReader ir) {

    }

    private static List<SearchResult> makeList(ImageSearchHits hits, IndexReader ir) throws IOException {
        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        for (int i = 0; i < hits.length(); i++) {
            Document document = ir.document(((SimpleImageSearchHits) hits).readerID(i));
            SearchResult result = new SearchResult();
            result.setPath(document.get("descriptorImageIdentifier"))
                    .setType(document.get("type"))
                    .setCategoryEnum(CategoryEnum.getInstance(document.get("category")))
                    .setScore(hits.score(i));
            log.info("Found something: " + result.toString());
            results.add(result);
        }
        return results;
    }

    private static Map<String, List<SearchResult>> getAllFeaturesResultMap(ImageSearcher searcher, IndexReader ir, BufferedImage img)
            throws IOException {

        Map<String, List<SearchResult>> resultsMap = new HashMap<>();

        for (Map.Entry<String, Class> entry : histogramMap.entrySet())
        {
            searcher = new GenericFastImageSearcher(10, entry.getValue());
            resultsMap.put(entry.getKey(), getResultList(searcher, ir, img));
        }

        return normalize(resultsMap);


    }

    private static Map<String, List<SearchResult>> normalize(Map<String, List<SearchResult>> resultMap) {


        for(List<SearchResult> current : resultMap.values()) {


            float A = 0;
            float B = 10;
            float a = 0;
            float b = 10;

            for(SearchResult result : current) {

                if(B - result.getScore() < 0) {
                    B *= 10;
                }

            }

            for(SearchResult result : current) {

                result.setScore(a + (result.getScore() - A) * (b - a) / (B - A));
            }
        }


        return resultMap;

    }

    public static ExtensiveSearchResult generateExtensiveSearchResult(Map<String, List<SearchResult>> resultList) {
        ExtensiveSearchResult exResult = new ExtensiveSearchResult();

        exResult.setAllFeaturesResultList(resultList);

        //TODO : all features most returned categories

        Map<String, String> mostCats = new HashMap<>();
        AtomicLongMap<String> overallCounts = AtomicLongMap.create();
        Map<String, AtomicLongMap<String>> typeCountPerCategory = new HashMap<>();


        for (Map.Entry<String, List<SearchResult>> entry : resultList.entrySet())
        {

            AtomicLongMap<String> counts = AtomicLongMap.create();
            for(SearchResult result : entry.getValue()) {
                counts.getAndIncrement(result.getCategoryEnum().category);
                overallCounts.getAndIncrement(result.getCategoryEnum().category);

                if(!typeCountPerCategory.containsKey(result.getCategoryEnum().category)) {
                    typeCountPerCategory.put(result.getCategoryEnum().category, AtomicLongMap.<String>create());
                } else {
                    typeCountPerCategory.get(result.getCategoryEnum().category).getAndIncrement(result.getType());
                }

            }

            Map<String, Long> countMap = counts.asMap();

            //Get largest count for this feature
            String category = "";
            Long catCount = 0L;
            for(Map.Entry<String, Long> innerEntry : countMap.entrySet()) {
                if(innerEntry.getValue() > catCount) {
                    catCount = innerEntry.getValue();
                    category = innerEntry.getKey();
                }
            }

            mostCats.put(entry.getKey(), category);
            exResult.setAllFeaturesMostReturnedCategories(mostCats);

        }

        Map<String, String> mostTypePerCategory = new HashMap<>();

        for(Map.Entry<String, AtomicLongMap<String>> typeCountEntry : typeCountPerCategory.entrySet()) {
            String type = "";
            Long typeCount = 0L;
            for(Map.Entry<String, Long> atomicEntry : typeCountEntry.getValue().asMap().entrySet()) {
                if(atomicEntry.getValue() > typeCount) {
                    typeCount = atomicEntry.getValue();
                    type = atomicEntry.getKey();
                }
            }
            mostTypePerCategory.put(typeCountEntry.getKey(), type);
        }

        //set overall count of categories
        exResult.setOverallMostReturnedCategories(overallCounts.asMap());
        exResult.setAllFeaturesMostTypesPerCategory(mostTypePerCategory);

        exResult.setAverageScoresPerCategory(getAverageScoreForCategories(resultList));

        return exResult;
    }

    private static Map<String, Float> getAverageScoreForCategories(Map<String, List<SearchResult>> resultList) {
        Map<String, Float> averageScores = new HashMap<>();
        List<Counter> counters = new ArrayList<>();

        //average scores are useless, as the more are returned, the more the score gets set lower,
        //in most cases.... maybe weigh from top to bottom so lower results weigh less....
        for (Map.Entry<String, List<SearchResult>> entry : resultList.entrySet()) {
            float weight = 1.1f;
            for(SearchResult result : entry.getValue()) {

                Counter counter = new Counter();
                counter.setCategory(result.getCategoryEnum().category);

                if(!counters.contains(counter)) {
                    counter.setSum(result.getScore() * weight);
                    counter.setCount(1);
                    counters.add(counter);
                } else {
                    Counter existingCounter = counters.get(counters.indexOf(counter));
                    existingCounter.add(result.getScore() * weight);
                }
                weight -= 0.1f;

            }
        }

        for(Counter counter : counters) {
            averageScores.put(counter.getCategory(), counter.getAverage());
        }

        return averageScores;

    }

}
