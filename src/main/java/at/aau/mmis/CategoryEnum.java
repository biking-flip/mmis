package at.aau.mmis;

/**
 * Created by phmoll on 15.06.15.
 */
public enum CategoryEnum {

    PERSON("Person", new String[]{"One Person", "Several people", "Portrait"}),
    ANIMAL("Animal", new String[]{"Bird", "Mammal", "Reptile", "Insect", "Aquatic"}),
    LANDSCAPE("Landscape", new String[]{"Urban", "Forest", "Plains", "Mountains", "Desert", "Coast", "Snowy", "General"}),
    CAR("Vehicle", new String[]{"Car", "Ship", "Plane", "Bike", "Bus", "Truck", "Train", "Tank"}),
    MISC("Misc", new String[]{"Drawing", "Logo", "Comic", "Painting of Person", "General Painting","Food", "Fruit", "Vegetable","Something else", "Text"}),
    MAP("Map", new String[]{"Map", "Satellite Image", "Ancient Map"}),
    BUILDING("Building", new String[]{"House", "Mansion", "Skyscraper", "Church", "Castle", "Some other building", "Interior", "City Building"}),
    PLANT("Plant", new String[]{"Flower", "Tree", "Bush"});

    String category;
    String[] types;

    CategoryEnum(String category, String[] strings) {
        this.category = category;
        this.types = strings;
    }

    public String getCategory() {
        return category;
    }

    public static String[] getCategories() {
        String[] categories = new String[values().length];
        for (int i = 0; i < values().length; i++) {
            categories[i] = values()[i].getCategory();
        }
        return categories;
    }

    public static CategoryEnum getInstance(String category) {
        for (CategoryEnum categoryEnum : values()) {
            if (categoryEnum.getCategory().equalsIgnoreCase(category)) {
                return categoryEnum;
            }
        }
        return null;
    }

    public String[] getTypes(String category) {
        CategoryEnum e = CategoryEnum.getInstance(category);
        return e.types;
    }
}
