package at.aau.mmis;

/**
 * Created by phmoll on 15.06.15.
 */
public class Constants {
    public static final String IMAGE_PATH_NAME = "images/buffer.jpg";
    public static final String IMAGE_DIRECTORY = "images/";
    public static final String INDEX_LOCATION = "index/index";
}
