package at.aau.mmis.beans;

/**
 * Created by clu on 6/22/15.
 */
public class Winner {

    String category;
    String type;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        Winner other = (Winner) obj;

        if(other != null) {
            return this.category.equals(other.category) && this.type.equals(other.type);
        }
        return false;

    }

    @Override
    public int hashCode() {
        return category.hashCode() + type.hashCode();
    }
}
