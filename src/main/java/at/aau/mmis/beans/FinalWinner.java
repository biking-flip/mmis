package at.aau.mmis.beans;

/**
 * Created by clu on 6/22/15.
 */
public class FinalWinner {

    boolean clearWinnerDetermined = false;
    String category;
    String type;
    String text;

    public boolean isClearWinnerDetermined() {
        return clearWinnerDetermined;
    }

    public void setClearWinnerDetermined(boolean clearWinnerDetermined) {
        this.clearWinnerDetermined = clearWinnerDetermined;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
