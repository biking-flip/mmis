package at.aau.mmis.beans;

/**
 * Created by clu on 6/22/15.
 */
public class Counter {

    String category;
    int count;
    float sum;


    public void inc() {
        count += 1;
    }

    public void add(float sum) {
        this.sum += sum;
        inc();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    @Override
    public int hashCode() {
        return category.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Counter other = (Counter) obj;
        if(this.category.equals(other.category)) {
            return true;
        }
        return false;
    }

    public Float getAverage() {
        return sum/count;
    }
}
