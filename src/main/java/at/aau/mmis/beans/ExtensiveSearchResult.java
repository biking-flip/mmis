package at.aau.mmis.beans;

import java.util.*;

/**
 * Created by clu on 6/21/15.
 */
public class ExtensiveSearchResult {

    Map<String, List<SearchResult>> allFeaturesResultList;

    Map<String, Long> overallMostReturnedCategories;

    Map<String, String> allFeaturesMostReturnedCategories;

    Map<String, String> allFeaturesMostTypesPerCategory;

    String overallHighestScoringCategory;

    Map<String, Float> averageScoresPerCategory;

    SearchResult winner;

    List<Winner> winnerList = new ArrayList<>();




    public SearchResult getWinner() {
        return winner;
    }

    public void setWinner(SearchResult winner) {
        this.winner = winner;
    }

    public Map<String, Float> getAverageScoresPerCategory() {
        return averageScoresPerCategory;
    }

    public void setAverageScoresPerCategory(Map<String, Float> averageScoresPerCategory) {
        this.averageScoresPerCategory = averageScoresPerCategory;
    }

    public Map<String, List<SearchResult>> getAllFeaturesResultList() {
        return allFeaturesResultList;
    }

    public void setAllFeaturesResultList(Map<String, List<SearchResult>> allFeaturesResultList) {
        this.allFeaturesResultList = allFeaturesResultList;
    }

    public Map<String, String> getAllFeaturesMostReturnedCategories() {
        return allFeaturesMostReturnedCategories;
    }

    public void setAllFeaturesMostReturnedCategories(Map<String, String> allFeaturesMostReturnedCategories) {
        this.allFeaturesMostReturnedCategories = allFeaturesMostReturnedCategories;
    }

    public Map<String, String> getAllFeaturesMostTypesPerCategory() {
        return allFeaturesMostTypesPerCategory;
    }

    public void setAllFeaturesMostTypesPerCategory(Map<String, String> allFeaturesMostTypesPerCategory) {
        this.allFeaturesMostTypesPerCategory = allFeaturesMostTypesPerCategory;
    }

    public String getOverallHighestScoringCategory() {
        return overallHighestScoringCategory;
    }

    public void setOverallHighestScoringCategory(String overallHighestScoringCategory) {
        this.overallHighestScoringCategory = overallHighestScoringCategory;
    }

    public Map<String, Long> getOverallMostReturnedCategories() {
        return overallMostReturnedCategories;
    }

    public void setOverallMostReturnedCategories(Map<String, Long> overallMostReturnedCategories) {
        this.overallMostReturnedCategories = overallMostReturnedCategories;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        sb.append("Features favorite categories:\n\n");

        for(Map.Entry<String, String> entry : allFeaturesMostReturnedCategories.entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        sb.append("\n" + getCatConsensus());

        sb.append("\n---------------------------------------\n");
        sb.append("Overall most returned categories: \n\n");

        for(Map.Entry<String, Long> entry : overallMostReturnedCategories.entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        sb.append("\n---------------------------------------\n");
        sb.append("Most Types returned per category: \n\n");

        for(Map.Entry<String, String> entry : allFeaturesMostTypesPerCategory.entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        sb.append("\n---------------------------------------\n");
        sb.append("Average scores per category: \n\n");

        for(Map.Entry<String, Float> entry : averageScoresPerCategory.entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        sb.append("\n---------------------------------------\n");
        sb.append("Winner by:\n");
        sb.append("Highest Single Score: " + winner.getCategoryEnum().getCategory() + ", " + winner.getType() + ", " +winner.getScore() + "\n");
        sb.append("Consensus: " + getConsensusFullResult() + "\n");
        sb.append("Highest Average Score: " + getHighestAverageScoreWinner() + "\n");
        sb.append("Overall most returned category: " + getHighestReturnedCategory() + "\n");
        sb.append("WinnerListSize:" + winnerList.size());

        return sb.toString();
    }

    public FinalWinner determineFinalWinner() {
        FinalWinner finalWinner = new FinalWinner();

        //count categories
        List<CatCount> catCount = new ArrayList<>();
        for(Winner winner : winnerList) {
            CatCount counter = new CatCount();
            counter.category = winner.getCategory();

            if(!catCount.contains(counter)) {
                counter.count = 1;
                catCount.add(counter);
            } else {
                catCount.get(catCount.indexOf(counter)).count += 1;
            }

        }

        Collections.sort(catCount);
        boolean threeSame;
        if(catCount.size() > 2) {
             threeSame = catCount.get(0).count == catCount.get(1).count && catCount.get(1).count == catCount.get(2).count;
        } else {
            threeSame = false;
        }

        if(catCount.size() == 1) {
            finalWinner.setCategory(catCount.get(0).category);
            finalWinner.setText(catCount.get(0).category);
        } else if(catCount.get(0).count > catCount.get(1).count) {
            finalWinner.setCategory(catCount.get(0).category);
            finalWinner.setText(catCount.get(0).category);
        } else if(!threeSame && catCount.get(0).count == catCount.get(1).count) {
            finalWinner.setCategory(null);
            finalWinner.setType(null);
            finalWinner.setText("Could be " + catCount.get(0).category + " or " + catCount.get(1).category + ", not sure...." );
            return finalWinner;
        } else {
            finalWinner.setCategory(null);
            finalWinner.setType(null);
            finalWinner.setText("We couldn't even agree on a category!");
            return finalWinner;
        }

        catCount = new ArrayList<>();

        //count types only for winner category
        for(Winner winner : winnerList) {
            if(winner.category.equals(finalWinner.category)) {
                CatCount counter = new CatCount();
                counter.category = winner.type;
                if(!catCount.contains(counter)) {
                    counter.count = 1;
                    catCount.add(counter);
                } else {
                    catCount.get(catCount.indexOf(counter)).count += 1;
                }
            }
        }

        Collections.sort(catCount);

        if(catCount.size() == 1) {
            finalWinner.setType(catCount.get(0).category);
            finalWinner.text += ", " + finalWinner.getType();
            finalWinner.clearWinnerDetermined = true;
        } else if (catCount.get(0).count > catCount.get(1).count) {
            finalWinner.setType(catCount.get(0).category);
            finalWinner.text += ", " + finalWinner.getType();
            finalWinner.clearWinnerDetermined = true;
        } else {
            finalWinner.text += ", maybe " + catCount.get(0).category + " or " + catCount.get(1).category + ", i'm not really sure.... ";
            finalWinner.setType(null);
        }

        return finalWinner;


    }

    public void addWinner(String category, String type) {
        Winner winner = new Winner();
        winner.setCategory(category);
        winner.setType(type);
        winnerList.add(winner);
    }

    private String getHighestReturnedCategory() {
        String category = "";
        long count = 0L;

        for(Map.Entry<String, Long> entry : overallMostReturnedCategories.entrySet()) {
            if(entry.getValue() > count) {
                category = entry.getKey();
                count = entry.getValue();
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(category + ", " + allFeaturesMostTypesPerCategory.get(category));

        addWinner(category, allFeaturesMostTypesPerCategory.get(category));

        return sb.toString();
    }

    private String getHighestAverageScoreWinner() {
        String category = "";
        Float score = 0f;
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String, Float> entry : averageScoresPerCategory.entrySet()) {
            if(entry.getValue() > score) {
                category = entry.getKey();
                score = entry.getValue();
            }
        }
        sb.append(category + ", ");
        sb.append(allFeaturesMostTypesPerCategory.get(category));
        sb.append(", " + score);

        addWinner(category, allFeaturesMostTypesPerCategory.get(category));

        return sb.toString();
    }

    private String getCatConsensus() {

        List<CatCount> countList = makeCatConsensusList();

        StringBuilder sb = new StringBuilder();
        sb.append("Consensus: ");


        if (countList.size() > 1) {
            if(countList.get(0).count > countList.get(1).count) {
                sb.append(countList.get(0).category);
            } else {
                sb.append(countList.get(0).category + "/" + countList.get(1).category);
            }
        } else {
            sb.append(countList.get(0).category);
        }

        return sb.toString();
    }

    private List<CatCount> makeCatConsensusList() {
        List<CatCount> countList = new ArrayList<>();
        for(Map.Entry<String, String> entry : allFeaturesMostReturnedCategories.entrySet()) {
            CatCount current = new CatCount();
            current.category = entry.getValue();
            current.count = 1;

            if(!countList.contains(current)) {
                countList.add(current);
            } else {
                countList.get(countList.indexOf(current)).count += 1;
            }
        }
        Collections.sort(countList);
        return countList;
    }

    private String getConsensusFullResult() {
        List<CatCount> countList = makeCatConsensusList();
        StringBuilder sb = new StringBuilder();

        if (countList.size() > 1) {
            if(countList.get(0).count > countList.get(1).count) {
                sb.append(countList.get(0).category);
                sb.append(" ,");
                sb.append(allFeaturesMostTypesPerCategory.get(countList.get(0).category));
                addWinner(countList.get(0).category, allFeaturesMostTypesPerCategory.get(countList.get(0).category));
            } else {
                sb.append("No Consensus!");
            }
        } else {
            sb.append(countList.get(0).category);
            sb.append(" ,");
            sb.append(allFeaturesMostTypesPerCategory.get(countList.get(0).category));
            addWinner(countList.get(0).category, allFeaturesMostTypesPerCategory.get(countList.get(0).category));
        }

        return sb.toString();
    }



    private class CatCount implements Comparable<CatCount> {

        String category;
        int count;

        @Override
        public boolean equals(Object obj) {
            CatCount other = (CatCount) obj;

            return this.category.equals(other.category);

        }

        @Override
        public int compareTo(CatCount o) {
            final int SMALLER = -1;
            final int EQUAL = 0;
            final int LARGER = 1;

            if(this.count > o.count)
                return SMALLER;
            if(this.count == o.count)
                return EQUAL;

            return LARGER;

        }
    }



}
