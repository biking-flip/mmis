package at.aau.mmis.beans;

import at.aau.mmis.CategoryEnum;

import java.awt.image.BufferedImage;
import java.util.Comparator;

/**
 * Created by phmoll on 15.06.15.
 */
public class SearchResult implements Comparable<SearchResult> {

    public CategoryEnum categoryEnum;
    public String type;
    public String path;
    public float score;

    @Override
    public int compareTo(SearchResult o) {
        return this.getScore() > o.getScore() ? -1 : 1;
    }

    @Override
    public String toString() {
        return "Weight: " + score +" [" + path + "] " + categoryEnum.getCategory() + " - " + type;
    }

    public CategoryEnum getCategoryEnum() {
        return categoryEnum;
    }

    public SearchResult setCategoryEnum(CategoryEnum categoryEnum) {
        this.categoryEnum = categoryEnum;
        return this;
    }

    public String getType() {
        return type;
    }

    public SearchResult setType(String type) {
        this.type = type;
        return this;
    }

    public String getPath() {
        return path;
    }

    public SearchResult setPath(String path) {
        this.path = path;
        return this;
    }

    public float getScore() {
        return score;
    }

    public SearchResult setScore(float score) {
        this.score = score;
        return this;
    }
}
