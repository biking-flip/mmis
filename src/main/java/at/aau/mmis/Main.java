package at.aau.mmis;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;

public class Main extends Application {


    private static Log log = LogFactory.getLog(Main.class);
    private Stage primaryStage;
    private BorderPane rootLayout;
    public ObservableList autofillData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;

//        initAutofillList();

        initRootLayout();

        WikipediaService.downloadRandomImage();

        showClassifier();
        showRecommender();
    }

//    private void initAutofillList() {
//        try {
//
//            if(DirectoryReader.indexExists(FSDirectory.open(new File(Constants.INDEX_LOCATION)))) {
//                IndexReader ir = DirectoryReader.open(FSDirectory.open(new File(Constants.INDEX_LOCATION)));
//
//                Terms terms = MultiFields.getTerms(ir, "type");
//                TermsEnum termsEnum = terms.iterator(null);
//                BytesRef text;
//
//                while((text = termsEnum.next()) != null) {
//                    autofillData.add(text.utf8ToString());
//                }
//
//                ir.close();
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    private void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("main.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showClassifier() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("view/classifier.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            Scene scene = rootLayout.getScene();
            scene.getStylesheets().add(ClassLoader.getSystemResource("control.css").toExternalForm());
            TabPane tabPane = (TabPane) scene.lookup("#tabPane");
            tabPane.getTabs().get(0).setContent(personOverview);
//            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            ClassifierController controller = loader.getController();
            controller.setMain(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showRecommender() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("view/recommender.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            ((TabPane)rootLayout.getScene().lookup("#tabPane")).getTabs().get(1).setContent(personOverview);
//            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            RecommenderController controller = loader.getController();
            controller.setMain(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
