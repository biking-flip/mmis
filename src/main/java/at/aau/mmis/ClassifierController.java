package at.aau.mmis;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import np.com.ngopal.control.AutoFillTextBox;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ClassifierController {

    private Main main;

    @FXML
    private Button newImageButton;

    @FXML
    private StackPane imageStackPane;

    @FXML
    private ImageView imageView;

    @FXML
    private ComboBox<String> categoryDrop;

    @FXML
    private ComboBox<String> typeInput;

//    @FXML
//    private AnchorPane imageAnchor;

    private void loadImage() {
        File file = new File(Constants.IMAGE_PATH_NAME);
        if (file.exists()) {
            file.delete();
        }

        imageView.setImage(WikipediaService.downloadRandomImageAndGetImageView().getImage());
//        imageView.setFitWidth(100);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);

        imageStackPane.setAlignment(imageView, Pos.CENTER);


    }

    @FXML
    private void saveImage() {
        String category = categoryDrop.getValue();
        String type = typeInput.getValue();

        if (category == null || category.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Category should not be empty", "Warning", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (type == null || type.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Type should not be empty", "Warning", JOptionPane.ERROR_MESSAGE);
            return;
        }

        LireService.indexImage(Constants.IMAGE_PATH_NAME, category, type);

        loadImage();
    }

    @FXML
    private void initialize() {

        loadImage();
        newImageButton.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                loadImage();
            }
        });

        categoryDrop.getItems().addAll(CategoryEnum.getCategories());

        categoryDrop.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                typeInput.getItems().clear();
                typeInput.getItems().addAll(CategoryEnum.getInstance(newValue).types);
            }
        });

        typeInput.getItems().addAll(CategoryEnum.getInstance(categoryDrop.getItems().get(0)).types);

    }

    public void setMain(Main main) {
        this.main = main;

    }
}
